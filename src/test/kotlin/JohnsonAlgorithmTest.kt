import java.lang.IllegalArgumentException
import org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

internal class JohnsonAlgorithmTest {

    @ParameterizedTest
    @MethodSource(value = ["arguments"])
    fun johnsonAlgorithm(edges: List<Edge>, verticesCount: Int, expected: List<List<Double>>) {
        val result = JohnsonAlgorithm(edges, verticesCount)
        assertThat(result, equalTo(expected))
    }

    @ParameterizedTest
    @MethodSource(value = ["arguments_throwable"])
    fun johnsonAlgorithm_throwable(edges: List<Edge>, verticesCount: Int, throwClass: Class<Throwable>) {
        assertThatExceptionOfType(throwClass)
            .isThrownBy { JohnsonAlgorithm(edges, verticesCount) }
    }

    companion object {
        @JvmField
        val edges = listOf(
            Edge(0, 1, -5.0),
            Edge(0, 2, 2.0),
            Edge(0, 3, 3.0),
            Edge(1, 2, 4.0),
            Edge(2, 3, 1.0)
        )

        @JvmField
        val edgesNegativeWeightCycle = listOf(
            Edge(0, 1, -5.0),
            Edge(0, 2, 2.0),
            Edge(0, 3, 3.0),
            Edge(1, 2, 4.0),
            Edge(2, 3, 1.0),
            Edge(3, 0, -50.0)
        )

        @JvmField
        val edgesOutOfUpperBounds = listOf(
            Edge(0, 1, -5.0),
            Edge(0, 2, 2.0),
            Edge(0, 3, 3.0),
            Edge(1, 2, 4.0),
            Edge(2, 6, 1.0)
        )

        @JvmField
        val edgesOutOfDownBounds = listOf(
            Edge(0, 1, -5.0),
            Edge(0, 2, 2.0),
            Edge(0, 3, 3.0),
            Edge(1, 2, 4.0),
            Edge(-2, 3, 1.0)
        )

        const val verticesCount = 4

        const val verticesCountNegative = -1

        const val verticesCountZero = 0

        @JvmField
        val expectedMatrix = listOf(
            listOf(0.0, 0.0, 0.0, 0.0),
            listOf(Double.MAX_VALUE, 0.0, 0.0, 0.0),
            listOf(Double.MAX_VALUE, Double.MAX_VALUE, 0.0, 0.0),
            listOf(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, 0.0)
        )

        @JvmStatic
        fun arguments() = listOf(
            Arguments.of(edges, verticesCount, expectedMatrix)
        )

        @JvmStatic
        fun arguments_throwable() = listOf(
            Arguments.of(edgesNegativeWeightCycle, verticesCount, IllegalArgumentException::class.java),
            Arguments.of(edgesOutOfUpperBounds, verticesCount, IndexOutOfBoundsException::class.java),
            Arguments.of(edgesOutOfDownBounds, verticesCount, IndexOutOfBoundsException::class.java),
            Arguments.of(edges, verticesCountNegative, IllegalArgumentException::class.java),
            Arguments.of(edges, verticesCountZero, IllegalArgumentException::class.java)
        )
    }
}
