import java.util.stream.Stream
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

internal class InsertionSortTest {

    @ParameterizedTest(name = "{index} => input: {0}, expected: {1}, comparator: {2}, description: {3}")
    @MethodSource(value = ["provider"])
    fun insertionSort(input: Array<Int>, expected: Array<Int>, comparator: Comparator<Int>, description: String) {
        assertArrayEquals(insertionSort(input, comparator), expected)
    }

    companion object {
        @JvmField
        val emptySource = emptyArray<Int>()

        @JvmField
        val oneElementSource = arrayOf(0)

        @JvmField
        val unsortedSource = arrayOf(91, 89, -59, 77, -23, -93, -60, -62, 87, -65)

        @JvmField
        val evenSource = arrayOf(91, 89)

        @JvmField
        val evenSorted = arrayOf(89, 91)

        @JvmField
        val oddSource = arrayOf(-59, 77, -23)

        @JvmField
        val oddSorted = arrayOf(-59, -23, 77)

        @JvmField
        val ascSource = arrayOf(-93, -65, -62, -60, -59, -23, 77, 87, 89, 91)

        @JvmField
        val descSource = ascSource.reversedArray()

        @JvmField
        val descComparator: Comparator<Int> = Comparator { t1, t2 -> t2.compareTo(t1) }

        @JvmField
        val ascComparator: Comparator<Int> = descComparator.reversed()

        @JvmStatic
        private fun provider(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(emptySource, emptySource, descComparator, "Empty array, desc comparator"),
                Arguments.of(emptySource, emptySource, ascComparator, "Empty array, asc comparator"),

                Arguments.of(oneElementSource, oneElementSource, descComparator, "One element array, desc comparator"),
                Arguments.of(oneElementSource, oneElementSource, ascComparator, "One element array, asc comparator"),

                Arguments.of(unsortedSource, descSource, descComparator, "More than one element array, desc comparator"),
                Arguments.of(unsortedSource, ascSource, ascComparator, "More than one element array, asc comparator"),

                Arguments.of(evenSource, evenSorted, ascComparator, "Even number of elements in array, asc comparator"),
                Arguments.of(oddSource, oddSorted, ascComparator, "Odd number of elements in array, asc comparator")
            )
        }
    }
}
