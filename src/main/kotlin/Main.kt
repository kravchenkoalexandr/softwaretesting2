fun main(args: Array<String>) {
    // Example
    val edges: List<Edge> = listOf(
        Edge(0, 1, -5.0),
        Edge(0, 2, 2.0),
        Edge(0, 3, 3.0),
        Edge(1, 2, 4.0),
        Edge(2, 3, 1.0)
    )

    val verticesCount = 4
    val distanceMatrix = JohnsonAlgorithm(edges, verticesCount)
    distanceMatrix.forEach { row ->
        println(row.joinToString(separator = " ") { w ->
            when {
                w == Double.MAX_VALUE -> "Inf".padEnd(5, ' ')
                w.isFinite() -> "%.2f".format(w).padEnd(5, ' ')
                else -> "Nan".padEnd(5, ' ')
            }
        })
    }
}
