fun <T : Comparable<T>> insertionSort(source: Array<T>, comparator: Comparator<T>): Array<T> {
    val result = source.copyOf()
    for (i in result.indices) {
        val value = result[i]
        var j = i - 1
        while (j >= 0 && comparator.compare(result[j], value) == 1) {
            result[j + 1] = result[j]
            --j
        }
        result[j + 1] = value
    }
    return result
}
