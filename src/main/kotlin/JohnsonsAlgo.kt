/**
 * A class that represents an directed edge of a graph with weight [w].
 * An edge starts at a vertex with index [u] and ends at a vertex with index [v]
 *
 * @param u starting vertex index
 * @param v end vertex index
 * @param w edge weight
 */
data class Edge(val u: Int, val v: Int, val w: Double)

/**
 * Johnson Algorithm Implementing Function.
 * Johnson's algorithm is a way to find the shortest paths between all pairs of vertices in an edge-weighted, directed graph.
 * It allows some of the edge weights to be negative numbers, but no negative-weight cycles may exist.
 * It works by using the Bellman–Ford algorithm to compute a transformation of the input graph that removes all negative weights,
 * allowing Dijkstra's algorithm to be used on the transformed graph.
 *
 * @param edges list of graph edges
 * @param verticesCount number of graph vertices
 *
 * @return length matrix of the shortest distances between all vertices of the graph
 *
 * @throws IllegalArgumentException if vertices count [verticesCount] is not greater than zero
 * @throws IllegalArgumentException if the graph contains a negative weight cycle
 * @throws IndexOutOfBoundsException
 *  if one of the vertices of the edges of the graph does not belong to the range of graph indices [0; [verticesCount])
 */
fun JohnsonAlgorithm(edges: List<Edge>, verticesCount: Int): List<List<Double>> {
    // TODO:
    //  - Create a class containing graph construction logic.
    //  - Delegate a graph structure check to the class that creates the graph.
    //  - Create special exceptions for irregular graph structure.
    // Verification vertices count
    require(verticesCount > 0) { "Vertices count must be greater than zero" }
    // Verification the indices of the vertices of the edges
    edges.forEach { edge ->
        with((0 until verticesCount)) {
            if (!contains(edge.u) || !contains(edge.v)) {
                throw IndexOutOfBoundsException("Edge: $edge, range of indices: [0; $verticesCount)")
            }
        }
        require(edge.w != Double.MAX_VALUE) { "Edge weight cannot be equal to Double.MAX_VALUE" }
    }
    // Check size
    if (verticesCount == 0) { return emptyList() }
    if (verticesCount == 1) { return listOf(listOf(edges.first().w)) }
    // Adding a new vertex s with edges to all the original vertices
    val s: Int = verticesCount
    val newEdges: List<Edge> = edges.toMutableList().apply {
        (0 until verticesCount).forEach { add(Edge(s, it, 0.0)) }
    }.toList()
    // Finding the shortest path from vertex s to source vertices (Bellman-Ford algorithm)
    val distances: List<Double> = BellmanFordAlgorithm(
        vertexIndex = s,
        edges = newEdges,
        verticesCount = verticesCount + 1).dropLast(1)
    // Reweight all edges
    val modifiedEdges: List<Edge> = edges.map { (u, v, w) ->
        Edge(u, v, w + distances[u] - distances[v])
    }.toList()
    // Return shortest path matrix (Dijkstra algorithm)
    return (0 until verticesCount).map { index ->
        DijkstraAlgorithm(index, modifiedEdges, verticesCount)
    }
}

/**
 * Bellman-Ford Algorithm Implementing Function.
 * The Bellman–Ford algorithm is an algorithm that computes shortest paths from a single source vertex
 * to all of the other vertices in a weighted digraph.
 * It is capable of handling graphs in which some of the edge weights are negative numbers.
 *
 * @param edges list of graph edges
 * @param verticesCount number of graph vertices
 * @param vertexIndex vertex index for which the shortest path is being searched
 *
 * @return array of shortest distances to the vertex with index [vertexIndex]
 *
 * @throws IllegalArgumentException if the graph contains a negative weight cycle
 */
fun BellmanFordAlgorithm(vertexIndex: Int, edges: List<Edge>, verticesCount: Int): List<Double> {
    // Array of shortest path distances
    val distances = DoubleArray(verticesCount) { Double.MAX_VALUE }
    distances[vertexIndex] = 0.0

    // Shortest distance calculation
    (1 until verticesCount).forEach { _ ->
        edges.forEach { (u, v, w) ->
            if (distances[u] != Double.MAX_VALUE && (distances[u] + w < distances[v])) {
                distances[v] = distances[u] + w
            }
        }
    }

    // TODO: Add special exception for negative weight cycle in source graph
    // Check for negative-weight cycles
    edges.forEach { (u, v, w) ->
        require(!(distances[u] != Double.MAX_VALUE && (distances[u] + w < distances[v]))) {
            "Negative weight cycle for vertex: $u, edge: ${Edge(u, v, w)}"
        }
    }

    return distances.toList()
}

/**
 * Dijkstra Algorithm Implementing Function.
 * Dijkstra's algorithm is an algorithm for finding the shortest paths between nodes in a graph
 * Not able to handle graphs that have edges with negative weight.
 *
 * @param edges list of graph edges
 * @param verticesCount number of graph vertices
 * @param vertexIndex vertex index for which the shortest path is being searched
 *
 * @return array of shortest distances to the vertex with index [vertexIndex]
 */
fun DijkstraAlgorithm(vertexIndex: Int, edges: List<Edge>, verticesCount: Int): List<Double> {
    // List of shortest path distances
    val distances = MutableList(verticesCount) { Double.MAX_VALUE }
    // List of visited vertices
    val visited = MutableList(verticesCount) { false }
    distances[vertexIndex] = 0.0

    // Shortest distance calculation
    (0 until verticesCount).forEach { _ ->
        val index = minDistance(distances, visited)
        visited[index] = true
        edges.forEach { (u, v, w) ->
            if (!visited[v] && (u == index) && (distances[index] != Double.MAX_VALUE) && (distances[index] + w < distances[v])) {
                distances[v] = distances[index] + w
            }
        }
    }

    return distances.toList()
}

/**
 * Function to select the next unvisited vertex,
 * the distance to which is minimal among the remaining unvisited vertices.
 *
 * @param distance array of shortest distances to all vertices of the graph
 * @param visited array of labels visited graph vertices
 *
 * @return index of an unvisited vertex with a minimum distance
 *  If there is no suitable vertex, return -1
 */
fun minDistance(distance: List<Double>, visited: List<Boolean>): Int {
    return distance.withIndex()
        .filter { (index, _) -> !visited[index] }
        .minBy { (_, value) -> value }?.index ?: -1
}
